export class Comment{
    private _id: number;
    private _date: Date;
    private _author: string;
    private _content: string;
    
    constructor(id:number, author:string, content:string){
        this._id = id;
        this._author = author;
        this._content = content;
        this._date = new Date();
    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }

    public get date(): Date {
        return this._date;
    }
    public set date(value: Date) {
        this._date = value;
    }

    public get author(): string {
        return this._author;
    }
    public set author(value: string) {
        this._author = value;
    }

    public get content(): string {
        return this._content;
    }
    public set content(value: string) {
        this._content = value;
    }
}