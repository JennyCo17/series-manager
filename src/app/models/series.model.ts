import {Comment} from './comment.model';
export class Series{
    private _id: number;
    private _name: string;
    private _firstEpisodeRelease: Date;
    private _nbSeasons: number;
    private _description: string;
    private _critic: string;
    private _image: string;
    private _comments: Array<Comment>;
    
    
    constructor(id:number, name:string, firstEpRel:Date, 
        nbSeasons:number, desc:string, critic:string,
        image:string, comments:Array<Comment>){
            this._id = id;
            this._name = name;
            this._firstEpisodeRelease = firstEpRel;
            this._nbSeasons = nbSeasons;
            this._description = desc;
            this._critic = critic;
            this._image = image;
            this._comments = comments;
    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }

    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }

    public get firstEpisodeRelease(): Date {
        return this._firstEpisodeRelease;
    }
    public set firstEpisodeRelease(value: Date) {
        this._firstEpisodeRelease = value;
    }

    public get nbSeasons(): number {
        return this._nbSeasons;
    }
    public set nbSeasons(value: number) {
        this._nbSeasons = value;
    }

    public get description(): string {
        return this._description;
    }
    public set description(value: string) {
        this._description = value;
    }

    public get critic(): string {
        return this._critic;
    }
    public set critic(value: string) {
        this._critic = value;
    }

    public get image(): string {
        return this._image;
    }
    public set image(value: string) {
        this._image = value;
    }

    public get comments(): Array<Comment> {
        return this._comments;
    }
    public set comments(value: Array<Comment>) {
        this._comments = value;
    }
}