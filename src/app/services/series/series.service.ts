import { Injectable } from '@angular/core';
import { Series } from 'src/app/models/series.model';
import { Comment } from 'src/app/models/comment.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeriesService {

  seriesSubject = new Subject();
  private _series = [
    new Series(
      0, "Game of Thrones", new Date("2011-04-17"), 8,
      "Neuf familles nobles rivalisent pour le contrôle du Trône de Fer dans les sept royaumes de Westeros.",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis auctor dui, et molestie mi.",
      "https://vignette.wikia.nocookie.net/gameofthrones/images/3/30/GoTS8.jpg/revision/latest?cb=20190403002325",
      [
        new Comment(0, "Root", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis auctor dui, et molestie mi."),
        new Comment(1, "Jean-Jacque", "Commentaire")
      ]
    ),
    new Series(
      1, "Casa de papel", new Date("2017-05-02"), 4,
      "Huit voleurs font une prise d'otages dans la Maison royale de la Monnaie d'Espagne, tandis qu'un génie du crime manipule la police pour mettre son plan à exécution.",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis auctor dui, et molestie mi.",
      "https://media.senscritique.com/media/000018935876/source_big/La_Casa_de_Papel.jpg",
      [
        new Comment(2, "Root", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis auctor dui, et molestie mi.")
      ]
    ),
    new Series(
      2, "The Witcher", new Date("2019-12-20"), 1,
      "Hexer Geralt reprend la mission inachevée d'un autre sorceleur dans un royaume traqué par une bête féroce. Yennefer se forge un avenir magique au prix d'un terrible sacrifice.",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis auctor dui, et molestie mi.",
      "http://fr.web.img5.acsta.net/r_1920_1080/newsv7/19/12/06/10/22/53721130.jpg",
      []
    )
  ];
  public get series() {
    return this._series;
  }
  public set series(value) {
    this._series = value;
  }
  
  constructor() { }

  emitSeries(){
    this.seriesSubject.next(this.series);
  }

  getOne(id:number){
    let seriesToReturn = null;
    this.doActionOnData(this.series, 'id', id, item => {
      seriesToReturn = item;
    });

    return seriesToReturn;
  }

  addSeries(name:string, firstRelease:string, nbSeasons:number, description:string, critic:string, img:string){
    let newSeries = new Series(
      this.series[this.series.length-1].id + 1, 
      name,
      new Date(firstRelease), nbSeasons, description, critic, img, []
    );
    this.series.push(newSeries);
    this.emitSeries();
  }

  updateSeries(idSeries:number, name:string, firstEpRelease:string, nbSeasons:number, 
    description:string, critic:string, link:string){
    this.doActionOnData(this.series, 'id', idSeries, (item:Series) => {
      item.name = name;
      item.firstEpisodeRelease = new Date(firstEpRelease);
      item.description = description;
      item.critic = critic;
      item.image = link;
    });
    this.emitSeries();
  }

  deleteSeries(idSeries:number){
    this.doActionOnData(this.series, 'id', idSeries, (item:Series) => {
      let index = this.series.indexOf(item);
      this.series.splice(index, 1);
    });
    this.emitSeries();
  }

  doActionOnData(array, keyFromArray, valueToCompare, cb){
    for (let item of array) {
        if(item[keyFromArray] === valueToCompare){
          cb(item);
          break;
        }
    }
  }
}
