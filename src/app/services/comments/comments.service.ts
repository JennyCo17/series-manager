import { Injectable } from '@angular/core';
import { SeriesService } from '../series/series.service';
import { Subject } from 'rxjs';
import { Series } from 'src/app/models/series.model';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  commentsSubject = new Subject();

  constructor(private seriesService:SeriesService) { }

  emitComments(series:Series){
    this.commentsSubject.next(series.comments);
  }

  deleteComments(series:Series, idComment){
    this.seriesService.doActionOnData(this.seriesService.series, 'id', series.id, (item:Series) => {
      for (let com of item.comments) {
        if(com.id == idComment){
          let index = series.comments.indexOf(com);
          series.comments.splice(index, 1);
          break;
        }
      }
    });
    this.emitComments(series);
  }
}
