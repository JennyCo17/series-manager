import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuth:boolean;

  constructor() { }

  signIn(res){
    this.isAuth = true;
    res(true);
}

signOut(){
  this.isAuth = false;
}

checkAuth(username:string, password:string){

  return new Promise(
    (res, rej) => {
      if("Administrateur" == username && "azerty" == password){
        return this.signIn(res);
      }
      rej("Le nom d'utilisateur ou le mot de passe est incorrect");
    }
  )
}
}
