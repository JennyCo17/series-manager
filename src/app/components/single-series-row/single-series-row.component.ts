import { Component, OnInit, Input } from '@angular/core';
import { faInfo, faEdit, faCommentDots, faTrash, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { SeriesService } from 'src/app/services/series/series.service';

@Component({
  selector: '[app-single-series-row]',
  templateUrl: './single-series-row.component.html',
  styleUrls: ['./single-series-row.component.css']
})
export class SingleSeriesRowComponent implements OnInit {

  @Input() id:number;
  @Input() name:string;
  @Input() date:Date;

  detailUrl:string;
  editUrl:string;
  commentsUrl:string;

  faDetails = faInfoCircle;
  faEdit = faEdit;
  faComments = faCommentDots;
  faDelete = faTrash;

  constructor(private seriesService:SeriesService) { }

  ngOnInit(): void {
    this.detailUrl = "/series/" + this.id;
    this.editUrl = "/series/edit/" + this.id;
    this.commentsUrl = "/series/comments/" + this.id;
  }

  onDeleteSeries(){
    this.seriesService.deleteSeries(this.id);
  }
}
