import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSeriesRowComponent } from './single-series-row.component';

describe('SingleSeriesRowComponent', () => {
  let component: SingleSeriesRowComponent;
  let fixture: ComponentFixture<SingleSeriesRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSeriesRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSeriesRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
