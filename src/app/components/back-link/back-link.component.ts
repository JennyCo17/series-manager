import { Component, OnInit } from '@angular/core';
import { faCaretLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-back-link',
  templateUrl: './back-link.component.html',
  styleUrls: ['./back-link.component.css']
})
export class BackLinkComponent implements OnInit {

  faBack = faCaretLeft;

  constructor() { }

  ngOnInit(): void {
  }

}
