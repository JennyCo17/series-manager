import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { faPlusCircle, faPowerOff } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  faDisconnect = faPowerOff;

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

  onSignOut(){
    this.authService.signOut();
    this.router.navigate(['/auth']);
  }

}
