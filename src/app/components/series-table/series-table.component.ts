import { Component, OnInit, OnDestroy } from '@angular/core';
import { Series } from 'src/app/models/series.model';
import { Subscription } from 'rxjs';
import { SeriesService } from 'src/app/services/series/series.service';

@Component({
  selector: 'app-series-table',
  templateUrl: './series-table.component.html',
  styleUrls: ['./series-table.component.css']
})
export class SeriesTableComponent implements OnInit, OnDestroy {

  series:Array<Series>;
  seriesSubscription:Subscription;

  constructor(private seriesService:SeriesService) { }

  ngOnInit(): void {
    this.seriesSubscription = this.seriesService.seriesSubject.subscribe(
      (series:Array<Series>) => {
        this.series = series;
      }
    );

    this.seriesService.emitSeries();
  }

  ngOnDestroy(){
    this.seriesSubscription.unsubscribe();
  }
}
