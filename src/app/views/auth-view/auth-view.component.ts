import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-view',
  templateUrl: './auth-view.component.html',
  styleUrls: ['./auth-view.component.css']
})
export class AuthViewComponent implements OnInit {

  errorMsg:string;

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

  onSignIn(form: NgForm){
    this.authService.checkAuth(form.value.username, form.value.password).then(
      res => {
        this.router.navigate(['/series']);
      },
      err=> {
        this.errorMsg = err;
      }
    );
  }
}
