import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSeriesViewComponent } from './edit-series-view.component';

describe('EditSeriesViewComponent', () => {
  let component: EditSeriesViewComponent;
  let fixture: ComponentFixture<EditSeriesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSeriesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSeriesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
