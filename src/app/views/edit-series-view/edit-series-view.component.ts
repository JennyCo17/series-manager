import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SeriesService } from 'src/app/services/series/series.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Series } from 'src/app/models/series.model';

@Component({
  selector: 'app-edit-series-view',
  templateUrl: './edit-series-view.component.html',
  styleUrls: ['./edit-series-view.component.css']
})
export class EditSeriesViewComponent implements OnInit {

  editForm:FormGroup;
  series:Series;


  constructor(private seriesService:SeriesService, private route:ActivatedRoute, 
    private formBuilder:FormBuilder, private router:Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    this.series = this.seriesService.getOne(+id);
    this._initForm();
  }

  _initForm(){
    this.editForm = this.formBuilder.group({
      'name' : [this.series.name, Validators.required],
      'firstEpRelease' : [this.series.firstEpisodeRelease.toISOString().split('T')[0], Validators.required],
      'nbSeasons' : [this.series.nbSeasons, [Validators.required, Validators.min(1)]],
      'description' : [this.series.description, Validators.required],
      'critic' : [this.series.critic, Validators.required],
      'imageLink': [this.series.image, Validators.required]
    });
  }

  onEditForm(){
    const formValues = this.editForm.value;
    this.seriesService.updateSeries(this.series.id, formValues.name, formValues.firstEpRelease,
      +formValues.nbSeasons, formValues.description, formValues.critic, formValues.imageLink);
    this.router.navigate(['/series']);
  }
}
