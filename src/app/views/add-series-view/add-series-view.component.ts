import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SeriesService } from 'src/app/services/series/series.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-series-view',
  templateUrl: './add-series-view.component.html',
  styleUrls: ['./add-series-view.component.css']
})
export class AddSeriesViewComponent implements OnInit {

  addForm:FormGroup;

  constructor(private seriesService:SeriesService ,private formBuilder:FormBuilder, private router:Router) { }

  ngOnInit(): void {
    this._initForm();
  }

  _initForm(){
    this.addForm = this.formBuilder.group({
      'name' : ['', Validators.required],
      'firstEpRelease' : ['', Validators.required],
      'nbSeasons' : ['', [Validators.required, Validators.min(1)]],
      'description' : ['', Validators.required],
      'critic' : ['', Validators.required],
      'imageLink': ['', Validators.required]
    })
  }
  onAddForm(){
    const formValues = this.addForm.value;
    this.seriesService.addSeries(
      formValues.name,
      formValues.firstEpRelease,
      +formValues.nbSeasons,
      formValues.description,
      formValues.critic,
      formValues.imageLink
      );
    this.router.navigate(['/series']);
  }
}
