import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSeriesViewComponent } from './add-series-view.component';

describe('AddSeriesViewComponent', () => {
  let component: AddSeriesViewComponent;
  let fixture: ComponentFixture<AddSeriesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSeriesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSeriesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
