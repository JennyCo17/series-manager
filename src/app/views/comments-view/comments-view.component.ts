import { Component, OnInit } from '@angular/core';
import { Series } from 'src/app/models/series.model';
import { Comment } from 'src/app/models/comment.model';
import { Subscription } from 'rxjs';
import { SeriesService } from 'src/app/services/series/series.service';
import { CommentsService } from 'src/app/services/comments/comments.service';
import { ActivatedRoute } from '@angular/router';
import { faUser, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-comments-view',
  templateUrl: './comments-view.component.html',
  styleUrls: ['./comments-view.component.css']
})
export class CommentsViewComponent implements OnInit {

  series:Series;
  comments:Array<Comment>;
  commentsSubscription:Subscription;

  faUser = faUser;
  faDelete = faTrash;

  constructor(private seriesService:SeriesService, private commentsService:CommentsService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    this.series = this.seriesService.getOne(+id);
    this.commentsSubscription = this.commentsService.commentsSubject.subscribe(
      (comments:Array<Comment>) => {
        this.comments = comments;
      }
    );

    this.commentsService.emitComments(this.series);
  }

  onDeleteCom(id:number){
    this.commentsService.deleteComments(this.series, id);
  }

}
