import { Component, OnInit } from '@angular/core';
import { Series } from 'src/app/models/series.model';
import { SeriesService } from 'src/app/services/series/series.service';
import { ActivatedRoute } from '@angular/router';
import { faTicketAlt, faVideo, faCommentDots, faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-single-series-view',
  templateUrl: './single-series-view.component.html',
  styleUrls: ['./single-series-view.component.css']
})
export class SingleSeriesViewComponent implements OnInit {

  series:Series;
  faTicket = faTicketAlt;
  faVideo = faVideo;
  faComments = faCommentDots;
  faUser = faUser;

  constructor(private seriesService:SeriesService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    this.series = this.seriesService.getOne(+id);
  }

  getUrlBgImg(){
    return 'url('+this.series.image+')';
  }
}
