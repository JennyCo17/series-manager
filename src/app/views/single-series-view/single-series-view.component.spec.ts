import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSeriesViewComponent } from './single-series-view.component';

describe('SingleSeriesViewComponent', () => {
  let component: SingleSeriesViewComponent;
  let fixture: ComponentFixture<SingleSeriesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSeriesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSeriesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
