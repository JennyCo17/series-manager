import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { SeriesViewComponent } from './views/series-view/series-view.component';
import { SingleSeriesViewComponent } from './views/single-series-view/single-series-view.component';
import { CommentsViewComponent } from './views/comments-view/comments-view.component';
import { ErrorViewComponent } from './views/error-view/error-view.component';
import { AddSeriesViewComponent } from './views/add-series-view/add-series-view.component';
import { EditSeriesViewComponent } from './views/edit-series-view/edit-series-view.component';
import { AuthService } from './services/auth/auth.service';
import { SeriesService } from './services/series/series.service';
import { SeriesTableComponent } from './components/series-table/series-table.component';
import { SingleSeriesRowComponent } from './components/single-series-row/single-series-row.component';
import { HeaderComponent } from './components/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BackLinkComponent } from './components/back-link/back-link.component';
import { CommentsService } from './services/comments/comments.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthViewComponent,
    SeriesViewComponent,
    SingleSeriesViewComponent,
    CommentsViewComponent,
    ErrorViewComponent,
    AddSeriesViewComponent,
    EditSeriesViewComponent,
    SeriesTableComponent,
    SingleSeriesRowComponent,
    HeaderComponent,
    BackLinkComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [
    AuthService,
    SeriesService,
    CommentsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
