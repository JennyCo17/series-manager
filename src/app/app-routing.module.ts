import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { SeriesViewComponent } from './views/series-view/series-view.component';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { SingleSeriesViewComponent } from './views/single-series-view/single-series-view.component';
import { AddSeriesViewComponent } from './views/add-series-view/add-series-view.component';
import { EditSeriesViewComponent } from './views/edit-series-view/edit-series-view.component';
import { CommentsViewComponent } from './views/comments-view/comments-view.component';


const routes: Routes = [
  { path:'auth', component:AuthViewComponent },
  { path:'series', component:SeriesViewComponent, canActivate:[AuthGuardService] },
  { path:'series/add', component:AddSeriesViewComponent, canActivate:[AuthGuardService] },
  { path:'series/edit/:id', component:EditSeriesViewComponent, canActivate:[AuthGuardService] },
  { path:'series/comments/:id', component:CommentsViewComponent, canActivate:[AuthGuardService] },
  { path:'series/:id', component:SingleSeriesViewComponent, canActivate:[AuthGuardService] },
  { path:'', component:SeriesViewComponent, canActivate:[AuthGuardService] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
